' Set your settings
    strFileURL = "https://bintray.com/vszakats/generic/download_file?file_path=openssl-1.1.0h-win32-mingw.zip"
    strHDLocation = "c:\openssl.zip"

' Fetch the file
    Set objXMLHTTP = CreateObject("MSXML2.ServerXMLHTTP.6.0")

    objXMLHTTP.open "GET", strFileURL, false
    objXMLHTTP.send()

If objXMLHTTP.Status = 200 Then
Set objADOStream = CreateObject("ADODB.Stream")
objADOStream.Open
objADOStream.Type = 1 'adTypeBinary

objADOStream.Write objXMLHTTP.ResponseBody
objADOStream.Position = 0    'Set the stream position to the start

Set objFSO = CreateObject("Scripting.FileSystemObject")
If objFSO.Fileexists(strHDLocation) Then objFSO.DeleteFile strHDLocation
Set objFSO = Nothing

objADOStream.SaveToFile strHDLocation
objADOStream.Close
Set objADOStream = Nothing
End if

Set objXMLHTTP = Nothing
