@echo on

setlocal

set RULENAME="Allow WinRM HTTPS"
set "SSLURL=https://bintray.com/vszakats/generic/download_file?file_path=openssl-1.1.0h-win32-mingw.zip"
set "SSLZIP=openssl-1.1.0h-win32-mingw.zip"
set "SSLZIPPATH=%TEMP%\%SSLZIP%"
set "SSLUNZIPPATH=%TEMP%\openssl-1.1.0h-win32-mingw"



rem Create Firewall Exception if it doesnt exist already
netsh advfirewall firewall show rule name=%RULENAME% >nul
if not ERRORLEVEL 1 (
    echo - Rule %RULENAME% already exist.
) else (
    echo - Rule %RULENAME% not exist. Creating...
    netsh.exe advfirewall firewall add rule name=%RULENAME% protocol=tcp dir=in enable=yes action=allow profile=private,domain,public localport=5986
)






rem Download and extract openssl if it isnt available locally
if exist %SSLUNZIPPATH%\openssl.exe (
    echo - openSSL detected. Skip downloading openSSL.
) else (
    echo - openSSL missing. Downloading...
    cscript.exe download.vbs %SSLURL% %SSLZIP%
    cscript //B unzip.vbs %SSLZIPPATH%
)




rem Create self-signed certificate for WinRM if localhost selfsigned cert doesnt already exist
certutil -store My | find "CN=localhost"
if not ERRLEVEL 1 (
    echo - SSL Cert already exist. Skip SSL cert creation.
) else (
    echo - Creating SSL Cert

    rem CA priv & pub
    %SSLUNZIPPATH%\openssl.exe genrsa -out ca.key 4096
    %SSLUNZIPPATH%\openssl.exe req -new x509 -days 3650 -key ca.key -out ca.crt

    rem Self-signed cert priv key
    %SSLUNZIPPATH%\openssl.exe genrsa -out ia.key 4096

    rem Self-signed cert certificate signing request
    %SSLUNZIPPATH%\openssl.exe req -new -key ia.key -out ia.csr -config req.cnf

    rem CA signs cert request resulting in self-signed certificate
    %SSLUNZIPPATH%\openssl.exe x509 -req -days 365 -in ia.csr -CA ca.crt -CAkey ca.key -out ia.crt

    rem import key into Windows local machine store
    certutil -enterprise -f -v -AddStore "Root" ia.crt
)





rem Start WinRM if it isnt already started
for /F "tokens=3 delims=: " %%H in ('sc query "winrm" ^| findstr "        STATE"') do (
    if /I "%%H" NEQ "RUNNING" (
        echo - starting winrm service
        net start "winrm"
    )
)
