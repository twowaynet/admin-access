#!/usr/bin/env bash

vagrant up
vagrant rsync
vagrant provision

vagrant ssh -c "cd /vagrant && ./access.bat"  
result=$?

vagrant suspend

exit ${result}
